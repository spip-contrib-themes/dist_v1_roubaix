<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
    'dist_theme_roubaix_nom'         => "Thème Dist Roubaix",
    'dist_theme_roubaix_slogan'      => "Un thème gris, deux colonnes, calé à gauche",
    'dist_theme_roubaix_description' => ""
);

?>
